ARG PARENT_IMAGE
FROM $PARENT_IMAGE

RUN \
    set eux && \
    cd / && \
    ls -al && \
    cd schema-registry && \   
    mvn package -P standalone -DskipTests && \
    ls -al && \
    ls -al ./package-schema-registry/target/
